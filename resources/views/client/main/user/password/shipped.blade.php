@component('mail::message')
# Восстановление пароля

Кто-то, надеюсь, вы запросили сбросить пароль для вашей учетной записи на {{$_SERVER['REQUEST_SCHEME']."://".$_SERVER['REQUEST_URI']}} .


<p></p>
<p>код сброса</p>
<p style="background-color: #bdbdbd; padding: 0.5rem 1rem; color: #fff; font-size: 20px;">{{$url}}</p>

<!--@component('mail::button', ['url' => $url])-->
<!--Сбросить пароль-->
<!--@endcomponent-->

@endcomponent
