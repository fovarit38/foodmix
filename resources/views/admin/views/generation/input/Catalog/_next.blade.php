@php

    $model_old=$model;

    $model_name="Product";

    $model = app("\App\\" . $model_name);
    $sql_name = $model->getTable();
    $colum_list = Schema::getColumnListing($sql_name);

    $tbody = $model;
    if (in_array('sort', $colum_list) == true) {
        $tbody = $tbody->orderby("sort");
    }
    $tbody = $tbody->get();
    $thead = [];

    if(!is_null($model_old)){
      $tbody= $tbody->where("catalog_id",$model_old->id) ;
    }


    $meta_colum = \App\Model_meta::where("type", "table_catalog")->where("attachment", $model_name)->get();

    foreach ($meta_colum as $rows) {
        $colums = \App\Column_name::where("name_key", $rows->name_key)->first();
        $thead[$rows->name_key] = isset($colums->name) ? $colums->name : $rows->name_key;
    }
    $table_link = ["/admin/model/" . $model_name . "/", "id",(!is_null($model_old)?'?skill_id='.$model_old->id:'')];

@endphp

@if(!is_null($model_old))
    <div class="bodyMain">
        <div class="header">
            <div
                class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-2 border-bottom">
                <h1 class="h2">Продукция</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group">
                        <a href="{{url_custom("/admin/model/".$model_name."/0".(!is_null($model_old)?'?Product_id='.$model_old->id:''))}}"
                           class="btn btn-sm btn-outline-secondary waves-effect  ">Добавление записи</a>
                    </div>
                </div>
            </div>
        </div>

        @include('constituent_element.table.main')

    </div>
@endif



@php
    $model_name="Catalog";

    $model = app("\App\\" . $model_name);
    $sql_name = $model->getTable();
    $colum_list = Schema::getColumnListing($sql_name);

    $tbody = $model;
    if (in_array('sort', $colum_list) == true) {
        $tbody = $tbody->orderby("sort");
    }
    $tbody = $tbody->get();
    $thead = [];

    if(!is_null($model_old)){
      $tbody= $tbody->where("parent_id",$model_old->id) ;
    }


    $meta_colum = \App\Model_meta::where("type", "table_catalog")->where("attachment", $model_name)->get();

    foreach ($meta_colum as $rows) {
        $colums = \App\Column_name::where("name_key", $rows->name_key)->first();
        $thead[$rows->name_key] = isset($colums->name) ? $colums->name : $rows->name_key;
    }
    $table_link = ["/admin/model/" . $model_name . "/", "id",(!is_null($model_old)?'?parent_id='.$model_old->id:'')];

@endphp

@if(!is_null($model_old))
    <div class="bodyMain">
        <div class="header">
            <div
                class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-2 border-bottom">
                <h1 class="h2">Подкатегории</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group">
                        <a href="{{url_custom("/admin/model/".$model_name."/0".(!is_null($model_old)?'?parent_id='.$model_old->id:''))}}"
                           class="btn btn-sm btn-outline-secondary waves-effect  ">Добавление записи</a>
                    </div>
                </div>
            </div>
        </div>

        @include('constituent_element.table.main')

    </div>
@endif

@php
    $product_id  = isset($_REQUEST["parent_id"])? $_REQUEST["parent_id"]:'';
    $product_id  = is_null($model_old)? $product_id: $model->parent_id;
@endphp

<input type="hidden" name="parent_id_save" value="{{$product_id}}">
