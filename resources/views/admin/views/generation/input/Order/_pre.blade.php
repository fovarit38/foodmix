<div class="btnlink">

    @if($id!="0")

        <form method="post" action="{{url_custom('/admin/orders/edit/set_status')}}">
            @csrf
            <input name="id" type="hidden" value="{{$id}}">
            <button type="submit" name="status" value="accepted" class="btn btn-sm {{$model->status=="accepted"?"btn-success":"btn-outline-secondary"}}  waves-effect">
                Заказ обработан
            </button>
        </form>



        <form method="post" action="{{url_custom('/admin/orders/edit/set_status')}}" style="margin-left: 1rem;">
            @csrf
            <input name="id" type="hidden" value="{{$id}}">
            <button type="submit" name="status" value="done" class="btn btn-sm {{$model->status=="done"?"btn-success":"btn-outline-secondary"}}  waves-effect">
                Доставлен
            </button>
        </form>

        <form method="post" action="{{url_custom('/admin/orders/edit/set_status')}}" style="margin-left: 1rem;">

            @csrf
            <input name="id" type="hidden" value="{{$id}}">
            <button type="submit" name="status" value="cancelled" class="btn btn-sm {{$model->status=="cancelled"?"btn-success":"btn-outline-secondary"}} waves-effect"
                    role="button" href="#">
                Отменён
            </button>
        </form>

    @else
        <input type="hidden" name="status_save" value="0">

    @endif

</div>
<div class="asd" style="width: 100%;">
    <a href="{{url_custom('/admin/export/'.$id)}}" class="btn btn-info" style="margin-bottom: 1rem;">Выгрузить</a>
</div>
<style>
    .btnlink {
        display: flex;
        flex-wrap: wrap;
        margin: 1rem 0;
        align-items: center;
    }
</style>
