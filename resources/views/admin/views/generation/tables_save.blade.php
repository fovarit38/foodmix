@extends('views.layouts.app')

@section('content')

    <div>
        <div
            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-2 border-bottom">
            <h1 class="h2">{{GMN($model_name)}} : {{$id==0?"Добавление записи":"Обновление записи"}}</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <div class="btn-group ">
                    <a href="javascript:void(0);" onclick="$('#form_submit').submit()" type="submit"
                       class="btn btn-sm  btn-success waves-effect  px-4">Применить</a>
                </div>

            </div>
        </div>

        @if(View::exists('views.generation.input.'.$model_name.'._pre'))
            @include('views.generation.input.'.$model_name.'._pre')
        @endif

        <form id="form_submit" enctype="multipart/form-data" method="post" action="{{url_custom("/admin/update")}}"
              class="bodyMain">
            @csrf

            <input type="hidden" name="model_name" value="{{$model_name}}">
            <input type="hidden" name="id" value="{{$id}}">
            <input type="hidden" name="path" value="{{url_custom("/admin/model/".$model_name."/{id}")}}">

            <div class="catalo_box" style="width: 100%;">
                @foreach($model_db->meta("table_save")->where("attachment",$model_name)->orderby("sort","desc")->get() as $modelSing)
                    @php
                        $plaholde=column_rename($modelSing->name_key);
        if($plaholde=="orderPrice"){
$plaholde="Стоимость доставки";
        }
                if($plaholde=="minPrice"){
$plaholde="Минимальная стоимость доставки";
        }
                    @endphp
                    @if($modelSing->name_key=="images")
                        {{maskInput($errors,["name"=>$modelSing->name_key."_save","type"=>"file","attr"=>"select","placeholder"=>$plaholde],$model)}}
                    @elseif($modelSing->name_key=="fromTime" || $modelSing->name_key=="toTime")
                        {{maskInput($errors,["name"=>$modelSing->name_key."_save","type"=>"time","attr"=>"select","placeholder"=>$plaholde],$model)}}
                    @elseif($modelSing->name_key=="sites_name_key")
                        {{maskInput($errors,["name"=>$modelSing->name_key."_save","type"=>"text","attr"=>"select","placeholder"=>$plaholde],$model)}}
                    @else
                        {{maskInput($errors,["name"=>$modelSing->name_key."_save","type"=>"text","placeholder"=>$plaholde],$model)}}
                    @endif
                @endforeach
            </div>


            @if(View::exists('views.generation.input.'.$model_name.'._next'))
                @include('views.generation.input.'.$model_name.'._next')
            @endif

        </form>
    </div>

@endsection
