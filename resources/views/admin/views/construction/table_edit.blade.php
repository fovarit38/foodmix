@extends('views.layouts.app')

@section('content')

    <form method="post" action="{{url_custom("/admin/table_meta_update")}}">
        <div
            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-2 border-bottom">
            <h1 class="h2">Изменение Таблицы</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <div class="btn-group ">
                    <input type="submit" class="btn btn-sm  btn-success waves-effect  px-4" value="Применить">
                </div>
                <input type="hidden" name="model_name" value="{{$model_name }}">
            </div>
        </div>

        <div class="bodyMain">

            @csrf

            <div class="check_forms">
                <div class="check_forms_head">
                    <h3 class="head-sort">
                        Информация
                        <a href="javascript:void(null)" class="cli" data-closest=".check_forms">
                            <span class="oi" data-glyph="plus"></span>
                            <span class="oi" data-glyph="minus"></span>
                        </a>
                    </h3>
                </div>
                <div class="check_forms_body" style=" padding: 0 1rem;padding-bottom: 1rem; ">

                    {{maskInput($errors,["name"=>"name_save","type"=>"text","placeholder"=>"Название таблицы"],$tb_info)}}
                    {{maskInput($errors,["name"=>"icon_save","type"=>"text","placeholder"=>"Иконка"],$tb_info)}}
                    <p>
                        <a href="https://useiconic.com/open/" target="_blank">https://useiconic.com/open/</a> <b>Icon Font</b>
                    </p>
                </div>
            </div>


            <div class="check_forms" style="margin-top: 2rem;">
                <div class="check_forms_head">
                    <h3 class="head-sort">
                        Выводимые столбцы в каталог

                        <a href="javascript:void(null)" class="cli" data-closest=".check_forms">
                            <span class="oi" data-glyph="plus"></span>
                            <span class="oi" data-glyph="minus"></span>
                        </a>
                    </h3>

                </div>
                <div class="check_forms_body">

                    @foreach($columns as $keyx=>$colum)
                        <div class="inputGroup-check ">
                            <input type="checkbox" name="colum_list[]" id="radio{{$keyx+1}}" value="{{$colum}}"
                                   {{$model_db->meta("table_catalog")->where("name_key",$colum)->count()>0?"checked":""}} class="chbox"
                                   style="display:none"/>
                            <label for="radio{{$keyx+1}}" class="toggle"><span></span>
                                <div class="text_box">{{column_rename($colum)}}</div>
                            </label>
                        </div>
                    @endforeach
                </div>
            </div>


            <div class="check_forms" style="margin-top: 2rem;">
                <div class="check_forms_head">
                    <h3 class="head-sort">
                        В Вывод в меню
                        <a href="javascript:void(null)" class="cli" data-closest=".check_forms">
                            <span class="oi" data-glyph="plus"></span>
                            <span class="oi" data-glyph="minus"></span>
                        </a>
                    </h3>
                </div>
                <div class="check_forms_body">

                    <div class="inputGroup-check ">
                        <input type="checkbox" name="availability" id="availability" value="1"
                               {{$model_db->availability()->where("name_key","1")->count()>0?"checked":""}} class="chbox"
                               style="display:none"/>
                        <label for="availability" class="toggle"><span></span>
                            <div class="text_box">ДА</div>
                        </label>
                    </div>

                </div>
            </div>

            <div class="check_forms" style="margin-top: 2rem;">
                <div class="check_forms_head">
                    <h3 class="head-sort">
                        Выводимые столбцы в каталог
                        <a href="javascript:void(null)" class="cli" data-closest=".check_forms">
                            <span class="oi" data-glyph="plus"></span>
                            <span class="oi" data-glyph="minus"></span>
                        </a>
                    </h3>
                </div>
                <div class="check_forms_body">

                    @foreach($columns as $keyx=>$colum)


                        <div class="inputGroup-check ">
                            <input type="checkbox" name="colum_save[]" value="{{$colum}}" id="colum_save{{$keyx+1}}"
                                   {{$model_db->meta("table_save")->where("name_key",$colum)->count()>0?"checked":""}} class="chbox"
                                   style="display:none"/>
                            <label for="colum_save{{$keyx+1}}" class="toggle"><span></span>
                                <div class="text_box">{{column_rename($colum)}}</div>
                            </label>
                        </div>
                    @endforeach
                </div>
            </div>

        </div>
    </form>


@endsection
