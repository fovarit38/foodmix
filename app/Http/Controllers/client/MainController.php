<?php

namespace App\Http\Controllers\client;

use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Image;
use App\Http\Resources\ProductGroups as ProductGroupsResource;
use App\Mail\resetShipped;
use Mail;

class MainController extends Controller
{

    public function __construct()
    {
//        $url=$_SERVER['REQUEST_SCHEME']."://".$_SERVER['REQUEST_URI']."/ru/almaty/user/";

    }

    public function index()
    {

        if (isset($_REQUEST["update"])) {

            \App\Product::truncate();
            \App\Catalog::truncate();

            $xmls = send_xml();
            $smpb = json_encode($xmls->RK7Reference->Items);
            $smpb = json_decode($smpb, true);

            $smpb = $smpb["Item"];

            foreach ($smpb as $index => $imd) {
                if (stristr($imd["@attributes"]["CategPath"], 'Food Mix') !== FALSE) {
                    if ($imd["@attributes"]["Status"] == "rsActive") {
                        dd($imd["@attributes"]);
                        $products = new \App\Product;
                        $products->title = $imd["@attributes"]["Name"];
                        $products->path = str_replace("Food Mix\Меню\\", "", $imd["@attributes"]["CategPath"]);
                        $products->save();
                    }
                }
            }
//        dd(0);
//        $grups = \App\ProductGroups::get();

            $products = \App\Product::get();
            foreach ($products as $produc) {
                $path = explode("\\", $produc["path"]);
                $id_save = null;
                $paths = '';
                foreach ($path as $ispahg) {
                    $catalogs = \App\Catalog::where("title", $ispahg)->first();
                    $paths .= ($paths != '' ? '\\' : '') . $ispahg;

                    if (is_null($catalogs)) {
                        $catalogs = new \App\Catalog;
                        $catalogs->title = $ispahg;
                        $catalogs->slug = $ispahg;
                        $catalogs->path = $paths;
                        $catalogs->parent_id = $id_save;
                        $catalogs->save();
                    }
                    $id_save = $catalogs->id;
                }
                if (!is_null($id_save)) {
                    $produc->catalog_id = $id_save;
                    $produc->save();
                }

            }
        }

        return view('views.main');
    }

    public function catalog($path)
    {
        $catalog = \App\Catalog::where("path", $path)->first();

        $title = $catalog->title;
        return view('views.catalog', compact('title', 'catalog'));
    }

    public function basket()
    {
        $basket = 'Корзина';
        return view('views.basket', compact('basket'));
    }


}
