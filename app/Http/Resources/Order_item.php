<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Product as ProductResource;

class Order_item extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        $produc_save = \App\Product::find($this->product_id);
        return [
            'id' => $this->id,
            'images'=>(!is_null($produc_save)?$produc_save->images:''),
            'title' => $this->title,
            'price' => $this->price,
            'amount' => $this->amount,
            'allPrice' => $this->price * $this->amount,
            'product_id' => $this->product_id,
            'created_at' => $this->created_at->format('d/m/Y'),
        ];
    }
}
