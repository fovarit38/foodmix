<?php

namespace App;

use Auth;
use DB;
use Illuminate\Database\Eloquent\Model;
use App\Permission_user;


class Permission_date extends Model
{
    public static function check($table, $crud, $type, $page = 0)
    {


        $userGrup = Permission_user::check();
        $sqlDate = self::where("permision_group", $userGrup)->where("type", $type)->whereIn("table", [$table, "All"])->where("crud", $crud);

        $ac = 0;
        $acLoc = 0;

        if (isset($sqlDate)) {
            $sqlDate = $sqlDate->first();
            if (isset($sqlDate)) {
                if (($sqlDate->assess) == 1) {
                    $ac = 1;
                }
            }
            if ($sqlDate["original"]["table"] == "All") {
                $acces = self::where("permision_group", $userGrup)->where("type", $type)->whereIn("table", [$table])->where("crud", $crud)->first();
                if (isset($acces)) {
                    $ac = $acces->assess;
                }
            }
        }


        if ($ac == 1) {
            return true;
        }

        return false;

    }
}
